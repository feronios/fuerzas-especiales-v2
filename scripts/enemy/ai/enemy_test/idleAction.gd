extends "res://addons/godot-behavior-tree-plugin/bt_base.gd"

# Leaf Node
func tick(tick):
	if tick.actor.animator.get_current_animation()!="idle" and !tick.actor.is_attacking():
		tick.actor.animator.play("idle")

	return OK
