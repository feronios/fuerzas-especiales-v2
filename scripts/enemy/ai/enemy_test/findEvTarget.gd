extends "res://addons/godot-behavior-tree-plugin/bt_base.gd"

# Leaf Node
func tick(tick):
	
	var signo=sign(tick.actor.global_position.x-global.player.global_position.x)
	tick.actor.target=tick.actor.global_position+Vector2(signo*100,-10)
	if tick.actor.animator.get_current_animation()!="idle" and !tick.actor.is_attacking():
		tick.actor.animator.play("idle")

	return OK
