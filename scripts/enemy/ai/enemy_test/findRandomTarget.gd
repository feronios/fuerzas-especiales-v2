extends "res://addons/godot-behavior-tree-plugin/bt_base.gd"

# Leaf Node
func tick(tick):

	if tick.actor.find_random_point():
		return OK
	return FAILED
