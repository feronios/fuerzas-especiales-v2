extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
const JUMP_VEL=300
const GRAVITY = 650.0
const WALK_MAX = 200

# arbol ai
var blackboard
var behavior_tree

var animator
var target
var aggro=0
var state
# para hacer mirror del sprite
var container

# knock -> stand
var knockback=false
var standing=false
var knockingback=false


var velocity=Vector2()

#sombra
var shadow_res=preload("res://escenas/shadow/shadow.tscn")
var shadow


func _ready():
	blackboard=$BehaviorBlackboard
	behavior_tree=$BehaviorTree
	animator=$container/EnemySprite/EnemyAnim
	container=$container
	target=global_position+Vector2(0,40)
	state='random_move'
	set_physics_process(true)
func _physics_process(delta):
	if shadow==null:
	
		shadow=shadow_res.instance()
		get_parent().add_child(shadow)
		
	shadow.z_index=z_index-1
	#posicion de la sombra
	if knockback:
		
		shadow.global_position.x=global_position.x
	
	else:
		shadow.global_position=global_position+Vector2(0,40)
		
	
	if(!knockback and !standing):
		behavior_tree.tick(self,blackboard)
	
	if(knockback):
		knockback(delta)
	
	
		if ( global_position.y+41>=shadow.global_position.y):
		
			global_position=shadow.global_position-Vector2(0,40)
			standing = true
			knockback = false
			knockingback=false
			velocity=Vector2(velocity.x,0)
			
			animator.play("stand")

			
# Si el jugador esta en rango
func is_player_in_range():
	if abs(shadow.global_position.x-global.player.shadow.global_position.x)<70 and abs(shadow.global_position.y-global.player.shadow.global_position.y)<15:
		return true
	return false

# ckeckea si el enemigo esta atacando
func is_attacking():
	if (animator.get_current_animation() == "combo1"):
		return true
	else:
		return false

# funcion para moverse
func move():
	var vector= target-shadow.global_position
	if state=='attacking':
		WALK_MAX=260
	else:
		WALK_MAX=200
	move_and_slide(vector.normalized()*WALK_MAX)
	set_mirroring()
		
	if animator.get_current_animation()!="move":
		animator.play("move")

#checkea si esta en el target
func is_on_target():
	if (target.x-5<=shadow.global_position.x and shadow.global_position.x<=target.x+5):
		if (target.y-5<=shadow.global_position.y and shadow.global_position.y<=target.y+5):
			return true
			
	return false

#encuentra un target para atacar
func find_attack_target():
	var lado
	lado=sign(global.player.global_position.x-global_position.x)*-1
	var rand1=lado*60
	randomize()
	var rand2=rand_range(0,4)
	target=global.player.shadow.global_position+Vector2(rand1,rand2)
	

func play_idle():
	if animator.get_current_animation()!="idle" and !is_attacking():
		animator.play("idle")
		
	
func play_combo_1():
	if animator.get_current_animation()!="combo1":
		animator.play("combo1")

func set_mirroring():
	var vector= global.player.global_position-global_position	
	container.scale.x=sign(vector.x)*1
	

func reset_aggro():
	aggro=0

# encuentra target random para moverse
func find_random_point():
	var signo
	var signo2
	var point
	remove_player_list()
	play_idle()

	
	randomize()
	var rand1=rand_range(0,1)
	
	if rand1>0.5:
		signo=1
	else: 
		signo=-1
	
	randomize()
	rand1=rand_range(0,1)
	if rand1>0.5:
		signo2=1
	else:
		signo2=-1
	randomize()
	rand1=rand_range(60,250)
	
	
	point=shadow.global_position+Vector2(signo*rand1,signo2*10)
	
	var space = get_world_2d().direct_space_state
	
	#var results = space.intersect_point(point, 32, [], 2147483647)
	
	#for i in results:
	#	if i['collider'].name == 'limite':
	#		return FAILED
	
	var results= space.intersect_ray(shadow.global_position,point,[],2)
	
	if results.empty()==false: 
		print(results['collider'])
		if results['collider'].name=='limite':

			return false
	
	
	target=point	
	return true

func find_point_inside():
	pass

# knockback 
func knockback(delta):
	
	var force=Vector2(1,0)
	if animator.get_current_animation()!="knockback":
		animator.play("knockback")
	if !knockingback:
		velocity.y = -JUMP_VEL
		knockingback=true
		state='random_move'
		target=shadow.global_position
		remove_player_list()
	#se suman las fuerzas en el eje x de velocidad y la gravedad
	force += Vector2(0, GRAVITY)
	
	# si ataco en el aire es necesario que el movimiento continue por lo que se suman las velocidades
	
	velocity += force * delta
	velocity.x=200*sign(global_position.x-global.player.global_position.x)
	#if(shadow.ray_collide("left") or shadow.ray_collide("right")):
	#	velocity.x=0
		
	
	#-----------------MOVIMIENTO-------------
	var motion = move_and_slide(velocity)

# se activa al terminar la animacion de pararse
func activate():
	play_idle()
	target=global_position+Vector2(0,40)
	standing=false

func remove_player_list():
	var me=global.player.enemy_list.find(self,0)
	if me!=-1:
		global.player.enemy_list.remove(me)

# cambia de estados
func change_state():
	if state=='random_move':
	
		randomize()
		var rand1=rand_range(0,1)
		
		if rand1>0.5:
			if global.player.allow_attack(self):
				state='attacking'
				return true
		
		else: 
			return false
	
	elif state=='attacking':
		
	
		
		state='random_move'
	
		return true