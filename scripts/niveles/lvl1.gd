extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process(true)
	global.spawn_player(Vector2(400,300))
	

func _process(delta):
	$CanvasLayer/Label.text="Debug: \n"+"can_attack: "+str($Player.can_attack)+"\nattacking: "+str($Player.attacking)
	global.sort_z()
	
	# Get the canvas transform
	var ctrans = get_canvas_transform()
	
	# The canvas transform applies to everything drawn,
	# so scrolling right offsets things to the left, hence the '-' to get the world position.
	# Same with zoom so we divide by the scale.
	var min_pos = -ctrans.get_origin()
	
	$battle_zone