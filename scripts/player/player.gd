extends KinematicBody2D

# Class controlling the player

# Movement Constants
const GRAVITY = 650.0
const WALK_MAX = 200
const STOP_FORCE = 1300
const STOP_COEFF = 0.65

const JUMP_VEL = 300#280
const MAX_AIR = 0.11 # Due to physics, character is always in the air. This is a tolerance


# Variables

# Movement
var velocity = Vector2()
var jumping = false
var can_jump = true
var falling = false
var attacking = false # player is attacking
var can_attack = true
var air_time = 100



# Input
var walk_left
var walk_right
var walk_up
var walk_down
var jump
var attack
var shoot
var change

# Spritework
var sees_left = false
var landed = true
var stopped = false
var player_sprite

# Death
var dead = false

#mirroring
var sprite

var shadow_res=preload("res://escenas/shadow/shadow.tscn")
var shadow

var enemy_list = []
func _ready():
	
	set_physics_process(true)
	set_process_input(true)
	
	player_sprite = $container/PlayerSprite/PlayerAnim
	

	
func _physics_process(delta):
	
		walk_left = Input.is_action_pressed("ui_left")
		walk_right = Input.is_action_pressed("ui_right")
		walk_up = Input.is_action_pressed("ui_up")
		walk_down = Input.is_action_pressed("ui_down")
		jump = Input.is_action_pressed("jump")

		if shadow==null:
			sprite=$container
			shadow=shadow_res.instance()
			get_parent().add_child(shadow)
		
		if (not dead):
			_movement(delta)

		
		
func _input(event):
	
		attack = event.is_action_pressed("attack")
		
		
	
func _movement(delta):

	var force = Vector2(0,0)
	
	# stop by default, inertia
	var stop = true
	# -----------MOVIMIENTO------------
	if (!is_attacking()):
		if (walk_left and !shadow.ray_collide("left")  and !walk_right):
				force+= Vector2(-1,0)
				
				stop = false
				stopped = false
				
				if (player_sprite.get_current_animation() != "move"  and !jumping && !falling && !attacking):
					player_sprite.play("move")
			
		if (walk_right and !walk_left):
	
			
				force+= Vector2(1,0)
				stop = false
				stopped = false
				
				if (player_sprite.get_current_animation() != "move" and !jumping && !falling && !attacking):
					player_sprite.play("move")
	
		if (walk_up and !walk_down and !shadow.ray_collide("up")):
	
			
				force+= Vector2(0,-1)
			
				stop = false
				stopped = false
			
				if (player_sprite.get_current_animation() != "move"  and !jumping && !falling && !attacking):
					player_sprite.play("move")
		
		if (walk_down and !walk_up and !shadow.ray_collide("down")):
	
			
				force+= Vector2(0,1)
				
				stop = false
				stopped = false
				if (player_sprite.get_current_animation() != "move" and !jumping && !falling && !attacking):
					player_sprite.play("move")
	
	
	if stop==true:
		stop()
	# Manage jumping
	if falling:
		if ( global_position.y+41>=shadow.global_position.y):

			global_position=shadow.global_position-Vector2(0,40)
			falling = false
			
			if (not landed):
				velocity=Vector2(velocity.x,0)
				landed = true
				player_sprite.play("idle")
				

	#-------------- CAMBIO DE JUMP A FALL -------------
	if (jumping and velocity.y>=0):
		
		falling = true
		jumping=false
		
	# ------------------CONTROL ANIMACION JUMP FALL ---------------
	if (velocity.y < 0 and player_sprite.get_current_animation() != "jump" and jumping and !attacking):
		
		player_sprite.play("jump")
		player_sprite.seek(0.3)
		

	if (falling and player_sprite.get_current_animation() != "fall" and !attacking):
		
		player_sprite.play("fall")
	
	
	# -------------------SALTO-------------------
	if (!jump):
		# puedo saltar si no he apretado el boton de saltar
		can_jump = true
	
	#si no estoy saltando y puedo saltar y aprete para saltar y no estoy callendo o atacando
	if (not jumping and can_jump and jump and !falling  and !attacking):
		
		landed=false
		can_jump = false
		jumping=true
		
		player_sprite.play("jump")
		
		velocity.y = -JUMP_VEL
	
	if (jumping or falling):
		#se suman las fuerzas en el eje x de velocidad y la gravedad
		force.x= force.x*WALK_MAX
		force += Vector2(0, GRAVITY)
		
		# si ataco en el aire es necesario que el movimiento continue por lo que se suman las velocidades
		if is_attacking():
			velocity += force * delta
			if(shadow.ray_collide("left") or shadow.ray_collide("right")):
				velocity.x=0
			
		# si no estoy atacando entonces puedo moverme libremente
		else:
			#esto es movimiento sin acelerar en el eje x por lo que la velocidad se iguala en x
			velocity.x=force.x
			# aqui es con aceleracion por la gravedad la velocidad tiene que sumarse a las anteriores para el eje y
			velocity.y+=force.y*delta
		
		# esto regula la velocidad al moverte mientras saltas
		if velocity.x>200 or velocity.x<-200:
			velocity.x=200*sign(velocity.x)
	
	else:	
			#si no estoy saltando entonces el movimiento es normal para todas las direcciones
			velocity = force.normalized()* WALK_MAX
			
	
	#-----------------MOVIMIENTO-------------
	var motion = move_and_slide(velocity)
	
	
	#----------------ATAQUE---------------
	if(!attack):
		
		can_attack=true
		
	elif(attack &&can_attack && !attacking):
	
		can_attack=false
		attack()
		
	
	
	# puedo atacar si la animacion termina
	if (attacking and !is_attacking()):
		
		attacking=false
	
		
	# -----------SPRITE MIRRORING---------------
	if (velocity.x>0):
		sees_left = false
	elif (velocity.x<0):
		sees_left = true
	if (sees_left):
		sprite.scale.x=-1
	else:
		sprite.scale.x=1

	# ----------- POSICION SHADOW ----------
	if jumping or falling:
		
		shadow.position.x=global_position.x
	else:
		shadow.position=global_position+Vector2(0,40)
	
		
#------------FUNCIONES-----------------

func stop():
	
	if velocity==Vector2(0,0):
		if (player_sprite.get_current_animation() != "idle" && !jumping && !falling &&!is_attacking()):
			
			if (not stopped and not jumping and not falling):
				
				stopped = true
				player_sprite.play("idle")



# combo de ataques normal
func attack():
		
		attacking = true
		if falling or jumping:
			player_sprite.play("jump_attack")
		elif !jumping:
				
			if (player_sprite.get_current_animation() != "combo1" and player_sprite.get_current_animation() != "combo2" and player_sprite.get_current_animation() != "combo3"):
				player_sprite.play("combo1")
			
			elif (player_sprite.get_current_animation() == "combo1"):
				player_sprite.play("combo2")
			
			elif (player_sprite.get_current_animation() == "combo2"):
				player_sprite.play("combo3")


func slow_attack():
	attacking=false
	
#funcion para checkear si el personaje esta atacando
func is_attacking():
	if (player_sprite.get_current_animation() == "combo1" or player_sprite.get_current_animation() == "combo2" or player_sprite.get_current_animation() == "combo3" or
	player_sprite.get_current_animation()=="jump_attack"):
		return true
	else:
		return false


func _on_PlayerAnim_animation_finished( anim_name ):
	player_sprite.play("idle")



func _on_combo_hitbox_body_entered( body ):
	if body.is_in_group('basic_enemy'):
		body.knockback=true
		
func allow_attack(enemy):
	if enemy_list.size()<1:
		enemy_list.append(enemy)
		return true
	return false