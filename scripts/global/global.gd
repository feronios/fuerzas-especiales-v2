extends Node

var p_scene=preload("res://escenas/player/player.tscn")
var player
var current_scene = null

func _ready():
        var root = get_tree().get_root()
        current_scene = root.get_child( root.get_child_count() -1 )

func spawn_player(pos):
	player=p_scene.instance()
	current_scene.add_child(player)
	player.position=pos
	
# esto se puede optimizar al guardar la prosicion previa y la actual asi solo hacer sort a los nodos que cambiaron su posicion
func sort_z():
	var nodes=get_tree().get_nodes_in_group('sortable')
	for i in nodes:
		i.z_index=i.global_position.y